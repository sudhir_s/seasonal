package com.appnomic.stats.stl;

public class Periodogram {

    public static final double MIN_FFT_CYCLES = 3.0;
    public static final int MAX_FFT_PERIOD = 512;

    public double[] periodogramPeaks(double[] data) {
        return periodogramPeaks(data, 4, null, 0.90);
    }

    public double[] periodogramPeaks(double[] data, Integer minPeriod, Integer maxPeriod, double thresh) {
        return null;
    }
}
