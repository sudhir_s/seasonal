package com.appnomic.stats.stl;

import org.apache.commons.math3.stat.StatUtils;

import java.util.Arrays;

public class Trend {

    private Periodogram periodogram = new Periodogram();

    public double[] fitTrend(double[] data, String kind, Integer period) {
        double[] trend = new double[data.length];
        if (kind == null) {
            Arrays.fill(trend, 0.0);
            return trend;
        }

        return null;
    }

    public double[] fitTrend(double[] data, String kind, Integer period, Integer pTimes) {
        double[] trend = new double[data.length];
        if (kind == null) {
            double mean = StatUtils.mean(data);
            Arrays.fill(trend, mean);
            return trend;
        }
        if (period == null) {

        }


        return null;
    }

    private int guessTrendedPeriod(double[] data) {
        int maxPeriod = Math.min(data.length / 3, 512);
        double[] broad = fitTrend(data, "median", maxPeriod, 2);
        double[] tmp = new double[data.length];
        for (int i = 0; i < data.length; ++i)
            tmp[i] = data[i] - broad[i];
        double[] peaks = periodogram.periodogramPeaks(tmp);
        if (peaks == null) return maxPeriod;
        return 0;
    }

}
