package com.appnomic.stats.stl;

public class SeasonalTrend {

    private final double[] data;
    private Decomposition decomposition;
    private final String kind;
    private final Integer period;
    private final Float minVariance;
    private final Float periodogramThresh;
    private final Seasonal seasonal;
    private final Trend trend;

    SeasonalTrend(double[] data, String kind, Integer period, Float minVariance,
                  Float periodogramThresh, Seasonal seasonal, Trend trend) {
        this.data = data;
        this.kind = kind;
        this.period = period;
        this.minVariance = minVariance;
        this.periodogramThresh = periodogramThresh;
        this.seasonal = seasonal;
        this.trend = trend;
    }

    public static class Builder {

        private double[] data;
        private String kind = "spline";
        private Integer period = 2;
        private Float minVariance = 0.05f;
        private Float periodogramThresh = 0.05f;

        public Builder setKind(String kind) {
            this.kind = kind;
            return this;
        }

        public Builder setPeriod(Integer period) {
            this.period = period;
            return this;
        }

        public Builder setMinVariance(Float minVariance) {
            this.minVariance = minVariance;
            return this;
        }

        public Builder setPeriodogramThresh(Float periodogramThresh) {
            this.periodogramThresh = periodogramThresh;
            return this;
        }

        public Builder setData(double[] data) {
            this.data = data;
            return this;
        }

        public SeasonalTrend build() {
            return new SeasonalTrend(this.data, this.kind, this.period, this.minVariance,
                    this.periodogramThresh, new Seasonal(), new Trend());
        }

    }

    public static class Decomposition {
        public double[] getData() {
            return data;
        }

        public double[] getTrend() {
            return trend;
        }

        public double[] getSeasonal() {
            return seasonal;
        }

        public double[] getResiduals() {
            return residuals;
        }


        private final double[] data;
        private final double[] trend;
        private final double[] seasonal;
        private final double[] residuals;

        public Decomposition(double[] data) {
            this.data = data;
            int size = data.length;
            trend = new double[size];
            seasonal = new double[size];
            residuals = new double[size];
        }
    }

    public Decomposition decompose() {
        double[] trend = this.trend.fitTrend(this.data, this.kind, this.period);
        double[] season = this.seasonal.fitSeasons(this.data, this.kind, this.period, this.minVariance,
                this.periodogramThresh);
        this.decomposition = new Decomposition(data);
        Decomposition result = this.decomposition;
        return result;
    }
}
